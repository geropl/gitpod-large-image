package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

var (
	wait = flag.Duration("wait", 5*time.Minute, "how long to block the script")
	tick = flag.Duration("tick", time.Second, "how frequently to tick and log")
)

func main() {
	flag.Parse()

	log.Println(fmt.Sprintf("Waiting for %s", wait))

	wait := time.After(*wait)
	ticker := time.NewTicker(*tick)
	defer ticker.Stop()

	for {
		select {
		case <-wait:
			log.Println("Wait complete, exiting.")
			os.Exit(0)
		case <-ticker.C:
			log.Println(time.Now().UTC().String())
		}
	}
}
